Rails.application.routes.draw do
  resources :categories
  devise_for :users
  resources :events, except: [:show]
  resources :universities 
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  root :to => 'universities#index'
  get 'universities/index2'
  get 'events/saveEventCategory'
  get 'events/loadCategories'
  get 'events/:id', to: 'events#show'
  
  
  
end
