class Event < ApplicationRecord
	belongs_to :university
	has_many :category_events
	has_many :categories, through: :category_events
end
