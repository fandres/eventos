class EventsController < ApplicationController
  before_action :set_event, only: [:show, :edit, :update, :destroy]

  # GET /events
  # GET /events.json
  def index
    @events = Event.all
    if params[:idUniversity]!=nil
      session[:idUniversity]=params[:idUniversity]
      id=params[:idUniversity]
    else
      id=session[:idUniversity]
    end
    @university=University.find(id)
    @events = @university.events.order(fecha: :asc)
    @categorias=Category.all
    
  end

  def index2
    @events = Event.all.order(fecha: :asc)
  end
  # GET /events/1
  # GET /events/1.json
  def show
  end

  # GET /events/new
  def new
    @event = Event.new
  end

  # GET /events/1/edit
  def edit
  end
 def loadCategories
    @event=Event.find(params[:id_event])
    @categorias=Category.all
    respond_to do |format|
      format.js
    end
  end

  def saveEventCategory
    idEvent   = params[:event_id]
    idCategory = params[:category_id]
    rta           = params[:checked]
    if rta=="true"
      categoryEvent=CategoryEvent.new
      categoryEvent.event_id=idEvent
      categoryEvent.category_id=idCategory
      categoryEvent.save
    else
      categoryEvent=CategoryEvent.find_by(event_id: idEvent, category_id:idCategory)
      categoryEvent.destroy
    end
    respond_to do |format|
         format.js
    end
  end

  # POST /events
  # POST /events.json
  def create
    @event = Event.new(event_params)
    @event.university_id=session[:idUniversity]
    respond_to do |format|
      if @event.save
        format.html { redirect_to @event, notice: 'Event was successfully created.' }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    respond_to do |format|
      if @event.update(event_params)
        format.html { redirect_to @event, notice: 'Event was successfully updated.' }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url, notice: 'Event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:id_event, :nombre, :descripcion, :lugar, :fecha, :categoria, :link)
    end
end
