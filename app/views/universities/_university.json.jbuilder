json.extract! university, :id, :id_universidad, :nombre, :direccion, :telefono, :ciudad, :departamento, :created_at, :updated_at
json.url university_url(university, format: :json)
