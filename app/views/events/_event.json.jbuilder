json.extract! event, :id, :id_event, :nombre, :descripcion, :lugar, :fecha, :categoria, :created_at, :updated_at
json.url event_url(event, format: :json)
