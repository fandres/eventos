class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.string :id_event
      t.string :nombre
      t.string :descripcion
      t.string :lugar
      t.date :fecha
      t.string :categoria

      t.timestamps
    end
  end
end
