class CreateUniversities < ActiveRecord::Migration[5.1]
  def change
    create_table :universities do |t|
      t.string :id_universidad
      t.string :nombre
      t.string :direccion
      t.string :telefono
      t.string :ciudad
      t.string :departamento

      t.timestamps
    end
  end
end
