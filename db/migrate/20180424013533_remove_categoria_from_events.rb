class RemoveCategoriaFromEvents < ActiveRecord::Migration[5.1]
  def change
    remove_column :events, :categoria, :string
  end
end
